import { Directive, ElementRef, Renderer2, OnInit, HostListener, HostBinding, Input } from '@angular/core';

@Directive({
  selector: '[appBetterHighlight]'
})
export class BetterHighlightDirective implements OnInit {

  constructor(private elementRef:ElementRef,private render:Renderer2) { }

  @Input() defaultColor: string="green";
   @Input() highlightColor: string="blue";
  
  ngOnInit() {
   // this.render.setStyle(this.elementRef.nativeElement,"background-color","pink");
    this.backgroundcolor=this.defaultColor;
  }

  @HostBinding("style.background-color") backgroundcolor:string;

  @HostListener("mouseenter") mouseover(eventData:Event)
  {
    //this.render.setStyle(this.elementRef.nativeElement,"background-color","pink");
    this.backgroundcolor=this.highlightColor;
  }

  @HostListener("mouseleave") mouseleave(eventData:Event)
  {
   // this.render.setStyle(this.elementRef.nativeElement,"background-color","transparent");
   this.backgroundcolor=this.defaultColor;
  }
}
