import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { ServerService } from "./services/server.service";
import { HttpModule } from "@angular/http";

import {FormsModule} from "@angular/forms";
import { FieldplanComponent } from './fieldplan/fieldplan.component';

import {HeaderComponent} from "./header/header.component";
import { RecipesComponent } from './recipes/recipes.component';
import { RecipesListComponent } from './recipes/recipes-list/recipes-list.component';
import { RecipesDetailComponent } from './recipes/recipes-detail/recipes-detail.component';
import { RecipesItemComponent } from './recipes/recipes-list/recipes-item/recipes-item.component';
import { ShoppingListComponent } from './shopping-list/shopping-list.component';
import { ShoppingEditComponent } from './shopping-list/shopping-edit/shopping-edit.component';
import { BasicHighlightDirective } from './directives/basic-highlight.directive';
import { BetterHighlightDirective } from './directives/better-highlight.directive';
import { UnlessDirective } from './directives/unless.directive';
import { OddEvenComponent } from './examples/odd-even/odd-even.component';
import { DropDownDirective } from "./shared/dropdown.directive";



@NgModule({
  declarations: [
    AppComponent,
    FieldplanComponent,
    HeaderComponent,
    RecipesComponent,
    RecipesListComponent,
    RecipesDetailComponent,
    RecipesItemComponent,
    ShoppingListComponent,
    ShoppingEditComponent,
    BasicHighlightDirective,
    BetterHighlightDirective,
    UnlessDirective,
    OddEvenComponent,
    DropDownDirective,      
  ],
  imports: [
    BrowserModule,    
    HttpModule,
    FormsModule 
  ],
  providers: [ServerService],
  bootstrap: [AppComponent]
})
export class AppModule { }
