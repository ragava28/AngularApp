import { Directive, OnInit, HostBinding, HostListener } from "@angular/core";

@Directive({
 selector:'[appDropdown]'
})
export class DropDownDirective implements OnInit {
  constructor() { }

  ngOnInit() {
  }

  @HostBinding("class.open")
  
   isOpen=false;

   @HostListener("click") toggleOpen()
  {
    this.isOpen=!this.isOpen;
  }

}