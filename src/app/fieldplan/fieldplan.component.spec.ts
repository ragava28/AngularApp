import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FieldplanComponent } from './fieldplan.component';

describe('FieldplanComponent', () => {
  let component: FieldplanComponent;
  let fixture: ComponentFixture<FieldplanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FieldplanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FieldplanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
