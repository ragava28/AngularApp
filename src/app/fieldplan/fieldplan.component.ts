import { Component, OnInit } from '@angular/core';
import { ServerService } from "./../services/server.service";

@Component({
  selector: 'app-fieldplan',
  templateUrl: './fieldplan.component.html',
  styleUrls: ['./fieldplan.component.css']
})
export class FieldplanComponent implements OnInit {

 constructor( private serverService:ServerService ){}

  ngOnInit() {
  }

  getFieldPlans()
  {
    this.serverService.invokeService(this.getFieldPlabObject()).subscribe(
      (response)=> console.log(response),
      (error)=>console.log(error)
    );
  }

  gettestcall()
  {
  this.serverService.getRequest().subscribe(
      (response)=> console.log(response),
      (error)=>console.log(error)
    );
    
  }

  getFieldPlabObject()
  {
    
    return [
                {
                        "name": "Norway",
                        "taxregime": "Norway RT 4.09",
                        "regimepresent":"Default",
                        "comments": "New Eco Model",
                        "exprofileValues":[
                            {
                                "profile":"Oil",
                                "profilevalues": [
                                  {"year": "y1","value": "45"}, 
                                  {"year": "y2","value": "45"},  
                                  {"year": "y3","value": "45"},  
                                  {"year": "y4","value": "45"},  
                                  {"year": "y5","value": "45"},  
                                  {"year": "y6","value": "45"},  
                                  {"year": "y7","value": "45"},  
                                  {"year": "y8","value": "45"},  
                                  {"year": "y9","value": "45"},  
                                  {"year": "y10","value": "45"},  
                                  {"year": "y11","value": "45"},  
                                  {"year": "y12","value": "45"}
                                ]
                            },
                            {
                                "profile":"Gas",
                                "profilevalues": [
                                      {"year": "y1","value": "45"},                          
                                      {"year": "y2","value": "45"},  
                                      {"year": "y3","value": "45"},  
                                      {"year": "y4","value": "45"},  
                                      {"year": "y5","value": "45"},  
                                      {"year": "y6","value": "45"},  
                                      {"year": "y7","value": "45"},  
                                      {"year": "y8","value": "45"},  
                                      {"year": "y9","value": "45"},  
                                      {"year": "y10","value": "45"},  
                                      {"year": "y11","value": "45"},  
                                      {"year": "y12","value": "45"}
                                    ]
                            }
                        ],
                        "projectmanagement":5,
                        "engineeringdesign":15,
                        "capitalcost":0,
                        "dccost":700000,
                        "opcost":700000
                    }
            ]
  }
}
